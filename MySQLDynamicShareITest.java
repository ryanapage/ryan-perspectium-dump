import com.perspectium.logging.PerspectiumLogger;
import com.perspectium.replicator.ReplicatorAgent;
import com.perspectium.replicator.common.AMQPTestHarness;
import com.perspectium.replicator.common.ConfigurationFileHarness;
import com.perspectium.replicator.common.SQLTestHarness;
import com.perspectium.replicator.common.TableTestHarness;

import junit.framework.TestCase;

import org.junit.*;

public class MySQLDynamicShareITest extends TestCase {

	public static final String TEST_CONFIGURATION_FILE = "./src/test/conf/mySQLDynamicShare.xml";
	public static final String TICKET_JSON_FILE = "src/test/resources/ticket_insert.json";

	public static final String PSP_OUT_REPLICATOR_AUTOMATED_TEST = "psp.out.replicator.automated_test";

	public static final String DATABASE = "psp_db";
	public static final String TABLE = "ticket";
	public static final String TICKET_TABLE_SQL = "create_ticket.sql";
	public static final String TICKET_CHANGES_TABLE_SQL = "create_ticket_change.sql";

	public static final int SLEEP_TIME = 5000;
	public static final int MAX_TRIES = 200;

	private static String sys_id = "01719d520ff9464011acc3ace1050ee6";
	private static String ticket = "TKTTest0101";
	private ReplicatorAgent fReplicatorAgent;

	@Test
	public static void testSQLSharer() {

		try {
			doInsert();
			doUpdate();
			doDelete();
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	private static void doInsert() {

		System.out.println("Insert ticket into MySQL");
		String insert = String.format(
				"insert into %s.%s (sys_id, number, short_description) values('%s', '%s', 'a test ticket')", DATABASE,
				TABLE, sys_id, ticket);

		try {
			SQLTestHarness.createTable(DATABASE, TICKET_CHANGES_TABLE_SQL);

			Thread.sleep(SLEEP_TIME);
			SQLTestHarness.insert(DATABASE, insert);
			new AMQPTestHarness(PSP_OUT_REPLICATOR_AUTOMATED_TEST, "ticket.insert", MAX_TRIES);
			TableTestHarness.DroptTable("psp_db", "ticket_changes");
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			fail(e.getMessage());
		}
	}

	private static void doUpdate() {

		System.out.println("Updating ticket into MySQL");
		String update = String.format("update %s.%s set short_description='updated short desc' where sys_id='%s'",
				DATABASE, TABLE, sys_id);

		try {
			SQLTestHarness.createTable(DATABASE, TICKET_CHANGES_TABLE_SQL);

			Thread.sleep(SLEEP_TIME);
			SQLTestHarness.update(DATABASE, update);

			new AMQPTestHarness(PSP_OUT_REPLICATOR_AUTOMATED_TEST, "ticket.update", MAX_TRIES);
			TableTestHarness.DroptTable("psp_db", "ticket_changes");
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			fail(e.getMessage());
		}
	}

	private static void doDelete() {

		System.out.println("deleting ticket from MySQL");
		String delete = String.format("delete from %s.%s where sys_id='%s'", DATABASE, TABLE, sys_id);

		try {
			SQLTestHarness.createTable(DATABASE, TICKET_CHANGES_TABLE_SQL);

			Thread.sleep(SLEEP_TIME);
			SQLTestHarness.update(DATABASE, delete);

			new AMQPTestHarness(PSP_OUT_REPLICATOR_AUTOMATED_TEST, "ticket.delete", MAX_TRIES);
			TableTestHarness.DroptTable("psp_db", "ticket_changes");
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			fail(e.getMessage());
		}
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();

		System.out.println("Setup begin");
		new ConfigurationFileHarness(TEST_CONFIGURATION_FILE);

		System.out.println("Dropping ticket table if it exists");
		TableTestHarness.DroptTable("psp_db", "ticket");
		TableTestHarness.DroptTable("psp_db", "ticket_changes");

		System.out.println("Creating ticket table ");
		SQLTestHarness.createTable(DATABASE, TICKET_TABLE_SQL);

		System.out.println("Removing messages in test queue");
		new AMQPTestHarness(PSP_OUT_REPLICATOR_AUTOMATED_TEST);

		System.out.println("Starting the replicator agent");
		fReplicatorAgent = new ReplicatorAgent();
		fReplicatorAgent.start(null);

		System.out.println("Setup end");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown begin");
		fReplicatorAgent.stop(0);

		System.clearProperty(PerspectiumLogger.LOGGING_ALERT_PROPERTY);
		super.tearDown();
		System.out.println("tearDown end");
	}

}
