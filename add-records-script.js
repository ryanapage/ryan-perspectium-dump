//Service Now Create Records Script
// Disable the quota rules which would ordinarily block you from doing this

var qGR = new GlideRecord('sysrule_quota');

qGR.get('ed8f7131bf032100eeedfd00df0739e1');

qGR.sys_policy = "";

qGR.update();

qGR.active = false;

qGR.update();



// Then generate the records

var i = 0;

var init = 0;

var cap = 10000;



for (i = init; i < cap; i++) {

    var iGR = new GlideRecord('incident');
    // TODO: test logger capabilities for live progress updates
    //var logger = new PerspectiumLogger();

    iGR.initialize();

    iGR.setWorkflow(false);

    if (i % 10000 === 0) {
        // logger.log('USER CREATION PROGRESS: ' + Math.floor(i / cap * 100) + '%');
        gs.log('USER CREATION PROGRESS: ' + Math.floor(i / cap * 100) + '%');
    }

    iGR.short_description = "Test Incident: " + i;

    iGR.insert();

}



// Then re-enable the quota rules if you want

qGR.active = false;

qGR.update();